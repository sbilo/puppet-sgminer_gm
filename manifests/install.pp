class sgminer_gm::install inherits sgminer_gm {

  archive { "${sgminer_gm::install_dir}/sgminer-gm-5.5.5.zip":
    ensure       => present,
    extract      => true,
    extract_path => $sgminer_gm::install_dir,
    source       => "https://github.com/genesismining/sgminer-gm/releases/download/5.5.5/sgminer-gm.zip",
  }

  archive { "${sgminer_gm::install_dir}/sgminer-gm/sgminer":
    ensure      => present,
    extract     => false,
    source      => "https://github.com/genesismining/sgminer-gm/releases/download/5.5.5/sgminer_ubuntu64",
    user        => 'root',
    group       => 'root',
    require     => Archive["${sgminer_gm::install_dir}/sgminer-gm-5.5.5.zip"],
  }

  file { "${sgminer_gm::install_dir}/sgminer-gm/sgminer":
    ensure  => present,
    mode    => '0744',
    require => Archive["${sgminer_gm::install_dir}/sgminer-gm/sgminer"],
  }
}