class sgminer_gm::service inherits sgminer_gm {

  file { '/lib/systemd/system/sgminer-gm.service':
    ensure  => file,
    content => template('sgminer_gm/sgminer-gm.service.erb'),
    notify  => Exec['sgminer_gm:daemon-reload']
  }

  exec {'sgminer_gm:daemon-reload':
    command     => '/bin/systemctl daemon-reload',
    refreshonly => true,
    notify      => Service['sgminer-gm']
  }

  service { 'sgminer-gm':
    ensure => $sgminer_gm::enabled ? {
      true  => running,
      false => stopped,
    },
    enable => $sgminer_gm::enabled
  }
}