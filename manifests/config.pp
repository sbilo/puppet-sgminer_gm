class sgminer_gm::config inherits sgminer_gm {

  file { "${sgminer_gm::install_dir}/sgminer-gm/${coin}.conf":
    ensure  => file,
    content => template("sgminer_gm/${coin}.conf.erb"),
    owner   => 'root',
    group   => 'root',
    mode    => '0644',
    require => Archive["${sgminer_gm::install_dir}/sgminer-gm-5.5.5.zip"],
    notify  => Service['sgminer-gm']
  }
}