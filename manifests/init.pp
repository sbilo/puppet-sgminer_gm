class sgminer_gm(
  Boolean $enabled                  = true,
  Array[String] $env_vars           = [
    'GPU_FORCE_64BIT_PTR=1',
    'GPU_MAX_HEAP_SIZE=100',
    'GPU_USE_SYNC_OBJECTS=1',
    'GPU_MAX_ALLOC_PERCENT=100',
    'GPU_SINGLE_ALLOC_PERCENT=100'],
  Stdlib::AbsolutePath $install_dir = '/opt',
  String $coin                      = 'btg'
) {
  contain sgminer_gm::install
  contain sgminer_gm::config
  contain sgminer_gm::service

  Class['::sgminer_gm::install']
  -> Class['::sgminer_gm::config']
  ~> Class['::sgminer_gm::service']
}